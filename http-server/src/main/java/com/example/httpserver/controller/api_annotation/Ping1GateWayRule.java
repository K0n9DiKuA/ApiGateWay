package com.example.httpserver.controller.api_annotation;


import com.qyh.gateway.client.support.springmvc.GenerateShortLink;
import com.qyh.gateway.client.support.springmvc.Limit;
import com.qyh.gateway.client.support.springmvc.LimitType;
import com.qyh.gateway.client.support.springmvc.Mock;

import java.lang.annotation.*;


@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@GenerateShortLink
@Limit(value = {LimitType.IP, LimitType.PROVINCE}, ipConfig = {"127.0.0.1.1", "192.168.1.1.2"}, provinceConfig = {"重庆", "四川"})
@Mock("this api is developing , this is the mockValue ! thanks!")
public @interface Ping1GateWayRule {
}
