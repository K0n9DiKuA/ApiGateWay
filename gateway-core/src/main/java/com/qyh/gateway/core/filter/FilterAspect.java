package com.qyh.gateway.core.filter;

import java.lang.annotation.*;

/**
 * @description: 过滤器注解
 * @email 3161788646@qq.com
 * @author K0n9D1KuA
 * @date 2023/9/17 14:34
 * @version 1.0
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface FilterAspect {
    /**
     * 过滤器ID
     * @return
     */
    String id();

    /**
     * 过滤器名称
     * @return
     */
    String name() default "";

    /**
     * 排序
     * @return
     */
    int order() default 0;

}
