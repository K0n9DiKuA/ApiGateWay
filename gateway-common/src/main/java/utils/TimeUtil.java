package utils;

import java.util.concurrent.TimeUnit;

/**
 * @author K0n9D1KuA
 * @version 1.0
 * @description: 自定义时钟 减少用户态和内存态之间的切换
 * @email 3161788646@qq.com
 * @date 2023/9/17 14:31
 */

public final class TimeUtil {

    private static volatile long currentTimeMillis;

    static {
        currentTimeMillis = System.currentTimeMillis();
        Thread daemon = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    currentTimeMillis = System.currentTimeMillis();
                    try {
                        TimeUnit.MILLISECONDS.sleep(1);
                    } catch (Throwable e) {

                    }
                }
            }
        });
        daemon.setDaemon(true);
        daemon.setName("common-fd-time-tick-thread");
        daemon.start();
    }

    public static long currentTimeMillis() {
        return currentTimeMillis;
    }
}
