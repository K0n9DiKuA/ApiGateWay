package utils;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import java.nio.charset.StandardCharsets;

/**
 * @author K0n9D1KuA
 * @version 1.0
 * @description: 这段代码是一个哈希工具类，用于将长URL转换为短URL。具体解释如下：
 * <p>
 * URL_PREFIX是一个常量，表示短URL的前缀，默认为"/"。
 * BASE62_CHARS是一个包含62个字符的字符数组，用于将哈希值转换为62进制的字符串。这个字符数组包含了0-9、A-Z和a-z这62个字符。
 * longUrlToShortUrl是一个静态方法，用于将长URL转换为短URL。它接收一个CharSequence类型的参数charSequence，表示长URL的字符序列。
 * 首先，使用MurmurHash算法对charSequence进行哈希计算，得到一个128位的哈希码hashCode。
 * 然后，将hashCode转换为long类型的哈希值hashValue。如果hashValue为负数，则取其绝对值。
 * 接下来，创建一个StringBuilder对象sb，用于存储转换后的短URL。
 * 进入一个循环，直到hashValue变为0。在每次循环中，取hashValue除以62的余数，并根据余数从BASE62_CHARS数组中取出对应的字符，追加到sb中
 * 最后，将sb进行反转，并将URL_PREFIX和反转后的字符串拼接起来，得到最终的短URL。
 * <p>
 * 使用示例代码：
 * <p>
 * java
 * <p>
 * public class Main {
 * public static void main(String[] args) {
 * String longUrl = "https://www.example.com/very/long/url";
 * String shortUrl = HashUtils.longUrlToShortUrl(longUrl);
 * System.out.println("Long URL: " + longUrl);
 * System.out.println("Short URL: " + shortUrl);
 * }
 * }
 * <p>
 * 输出结果类似于：
 * <p>
     Long URL: https://www.example.com/very/long/url
     Short URL: /7p7kxuKEWEM
 * <p>
 * 这个示例演示了将长URL转换为短URL的过程，通过哈希计算和字符转换，将长URL转换为一个相对较短的字符串作为短URL的表示形式。
 * @email 3161788646@qq.com
 * @date 2023/9/17 14:24
 */

public class HashUtils {


    public static final String URL_PREFIX = "/";

    private static final char[] BASE62_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();

    public static String longUrlToShortUrl(CharSequence charSequence) {
        HashCode hashCode = Hashing.murmur3_128().hashString(charSequence, StandardCharsets.UTF_8);
        long hashValue = hashCode.asLong();
        hashValue = hashValue < 0 ? -hashValue : hashValue;
        StringBuilder sb = new StringBuilder();
        while (hashValue > 0) {
            sb.append(BASE62_CHARS[(int) (hashValue % 62)]);
            hashValue /= 62;
        }
        return URL_PREFIX + sb.reverse().toString();
    }


    public static void main(String[] args) {
        String longUrl = "https://www.example.com/very/long/url";
        String shortUrl = HashUtils.longUrlToShortUrl(longUrl);
        System.out.println("Long URL: " + longUrl);
        System.out.println("Short URL: " + shortUrl);
        //Long URL: https://www.example.com/very/long/url
        //Short URL: /7p7kxuKEWEM
    }

}
