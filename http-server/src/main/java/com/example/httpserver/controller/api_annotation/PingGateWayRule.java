package com.example.httpserver.controller.api_annotation;


import com.qyh.gateway.client.support.springmvc.*;
import constants.FilterConst;

import java.lang.annotation.*;

import static constants.FilterConst.FLOW_CTL_MODEL_DISTRIBUTED;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
//自动生成短链
@GenerateShortLink
//该接口禁止 ip 127.0.0.1 和 192.168.1.1 的 访问
//禁止 重庆 四川的省份访问
//@Limit(value = {LimitType.IP, LimitType.PROVINCE}, ipConfig = {"127.0.0.1", "192.168.1.1"}, provinceConfig = {"重庆", "四川"})
//表示该接口直接返回测试值
//@Mock("你好！ 该接口还没开发完成 ， 这是测试返回值！")
//标识这是一个灰度发布接口，会有 1 / 2048的概率被路由到灰度发布接口
//如果不指定 默认就是 1/1024
//@Gray(2048)
//选择哪种负载均衡策略？ 如果不指定 默认就是随机
@LoadBalance(FilterConst.LOAD_BALANCE_STRATEGY_RANDOM)
//设置超时重试次数为4次
//如果不设置超时次数 那么默认为5次
@RetryTimes(4)
//表示对参数名为 username 进行限流
//1s内允许通过的qps为5
//分布式限流
@HotParamControl(paramName = "username", windowCount = 20, qps = 2, controlType = FLOW_CTL_MODEL_DISTRIBUTED)
public @interface PingGateWayRule {
}
