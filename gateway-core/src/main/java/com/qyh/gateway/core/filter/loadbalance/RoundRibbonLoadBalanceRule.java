package com.qyh.gateway.core.filter.loadbalance;

import config.ServiceInstance;
import lombok.extern.slf4j.Slf4j;


import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;



/**
 * @description: 轮询策略
 * @email 3161788646@qq.com
 * @author K0n9D1KuA
 * @date 2023/9/17 14:35
 * @version 1.0
 */

@Slf4j
public class RoundRibbonLoadBalanceRule extends AbstractLoadBalanceRule {


    final AtomicInteger position = new AtomicInteger(0);

    //    String LOAD_BALANCE_STRATEGY_RANDOM = "Random";
    //    String LOAD_BALANCE_STRATEGY_ROUND_ROBIN = "RoundRobin";
    public static final String BALANCE_TYPE = "RoundRobin";


    @Override
    protected ServiceInstance doChoose(List<ServiceInstance> serviceInstanceList) {
        int pos = position.getAndIncrement();
        int size = serviceInstanceList.size();
        int takeIndex = pos % size;
        return serviceInstanceList.get(takeIndex);
    }

    @Override
    protected IGatewayLoadBalanceRule getObject() {
        return this;
    }

    @Override
    public boolean match(String type) {
        return BALANCE_TYPE.equals(type);
    }

}
