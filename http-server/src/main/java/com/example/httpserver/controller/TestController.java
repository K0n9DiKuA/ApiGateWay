package com.example.httpserver.controller;

import com.example.httpserver.controller.api_annotation.Ping1GateWayRule;
import com.qyh.gateway.client.support.springmvc.*;
import constants.FilterConst;
import constants.GatewayConst;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

import static constants.FilterConst.FLOW_CTL_MODEL_DISTRIBUTED;
import static constants.FilterConst.FLOW_CTL_MODEL_SINGLETON;


@RestController
public class TestController {


    //自动生成短链
    @GenerateShortLink
    //该接口禁止 ip 127.0.0.1 和 192.168.1.1 的 访问
    //禁止 重庆 四川的省份访问
    //@Limit(value = {LimitType.IP, LimitType.PROVINCE}, ipConfig = {"127.0.0.1", "192.168.1.1"}, provinceConfig = {"重庆", "四川"})
    //表示该接口直接返回测试值
    //@Mock("你好！ 该接口还没开发完成 ， 这是测试返回值！")
    //标识这是一个灰度发布接口，会有 1 / 2048的概率被路由到灰度发布接口
    //如果不指定 默认就是 1/1024
    //@Gray(2048)
    //选择哪种负载均衡策略？ 如果不指定 默认就是随机
    @LoadBalance(FilterConst.LOAD_BALANCE_STRATEGY_RANDOM)
    //设置超时重试次数为4次
    //如果不设置超时次数 那么默认为5次
    @RetryTimes(4)
    //表示对参数名为 username 进行限流
    //1s内允许通过的qps为5
    //分布式限流
    @HotParamControl(paramName = "username", windowCount = 20, qps = 2, controlType = FLOW_CTL_MODEL_DISTRIBUTED)
    @GetMapping("/http-server/ping")
    public String ping() throws InterruptedException {
        //模拟接口超时
//        Thread.sleep(1000000);
        return "pong";
    }

    @Ping1GateWayRule
    @GetMapping("/http-server/ping1")
    public String ping1() {
        return "pong1";
    }

    @GetMapping("/http-server/ping2")
    public String ping2() {
        return "pong2";
    }

}
