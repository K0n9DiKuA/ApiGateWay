package constants;

/**
 * @author K0n9D1KuA
 * @version 1.0
 * @description: 网关支持协议
 * @email 3161788646@qq.com
 * @date 2023/9/17 14:22
 */

public interface GatewayProtocol {

    String HTTP = "http";

    String DUBBO = "dubbo";

    static boolean isHttp(String protocol) {
        return HTTP.equals(protocol);
    }

    static boolean isDubbo(String protocol) {
        return DUBBO.equals(protocol);
    }

}
